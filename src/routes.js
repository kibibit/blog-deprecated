import home from './components/home.vue'
import blogs from './components/blogs.vue'
import contact from './components/contact.vue'

export const routes = [
    {path: '/', component: home},
    {path: '/blog', component: blogs},
    {path: '/contact', component: contact}
];